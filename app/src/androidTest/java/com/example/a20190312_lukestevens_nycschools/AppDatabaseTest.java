package com.example.a20190312_lukestevens_nycschools;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.a20190312_lukestevens_nycschools.db.AppDatabase;
import com.example.a20190312_lukestevens_nycschools.db.NYOpenDataDao;
import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import io.reactivex.observers.TestObserver;

@RunWith(AndroidJUnit4.class)
public class AppDatabaseTest {
    private NYOpenDataDao nyOpenDataDao;
    private AppDatabase appDatabase;

    @Before
    public void setup() {
        appDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getTargetContext(),
                AppDatabase.class).build();
        nyOpenDataDao = appDatabase.nyOpenDataDao();
    }

    @Test
    public void testDatabase_Write() {
        final String SCHOOL_NAME  = "Some School";

        HighSchool highSchool = new HighSchool();
        highSchool.setSchoolName(SCHOOL_NAME);

        nyOpenDataDao.insertHighSchools(highSchool);


        TestObserver<List<HighSchool>> testObserver = nyOpenDataDao.getAllHighSchools()
                .test();

        testObserver.assertValueAt(0, result -> result.get(0).getSchoolName().equals(SCHOOL_NAME));
    }

    @Test
    public void testDatabase_Empty() {
        TestObserver<List<HighSchool>> testObserver = nyOpenDataDao.getAllHighSchools()
                .test();

        testObserver.assertValueAt(0, List::isEmpty);
    }


    @After
    public void tearDown() {
        appDatabase.close();
    }
}
