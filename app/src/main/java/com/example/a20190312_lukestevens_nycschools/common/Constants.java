package com.example.a20190312_lukestevens_nycschools.common;

public class Constants {
    public static final String BASE_URL = "https://data.cityofnewyork.us/resource/";
    public static final String HIGHSCHOOLS_ENDPOINT = "s3k6-pzi2.json";
    public static final String SATRECORDS_ENDPOINT = "f9bf-2cp4.json";

    public static final long API_TIMEOUT = 30L;

    public static final int CACHE_SIZE = 10 * 1024 * 1024; //10 MB;

    public static final String TABLE_NAME = "highSchools";
    public static final String DATABASE_NAME = "NYOpenData";
    public static final int DATABASE_VERSION = 1;

    public static final String HIGHSCHOOL_KEY = "highschool";

}
