package com.example.a20190312_lukestevens_nycschools.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.a20190312_lukestevens_nycschools.common.Constants;
import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;

@Database(entities = {HighSchool.class}, version = Constants.DATABASE_VERSION)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NYOpenDataDao nyOpenDataDao();
}
