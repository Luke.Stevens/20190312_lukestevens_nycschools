package com.example.a20190312_lukestevens_nycschools.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;

import java.util.List;

import io.reactivex.Maybe;

@Dao
public interface NYOpenDataDao {
    @Query("SELECT * FROM highSchools")
    Maybe<List<HighSchool>> getAllHighSchools();

    //Using varargs to support both individual and collection of items
    @Insert
    void insertHighSchools(HighSchool... highSchools);

}
