package com.example.a20190312_lukestevens_nycschools.di;

import android.app.Application;

import com.example.a20190312_lukestevens_nycschools.common.App;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(modules = {AndroidInjectionModule.class, BuildersModule.class, NetworkModule.class,
        StorageModule.class, RepositoryModule.class})
@Singleton
public interface AppComponent {
    void inject(App app);

    @Component.Builder
    interface Builder {
        AppComponent build();
        @BindsInstance Builder application(Application application);
    }
}
