package com.example.a20190312_lukestevens_nycschools.di;

import com.example.a20190312_lukestevens_nycschools.ui.home.HomeActivity;
import com.example.a20190312_lukestevens_nycschools.ui.home.di.HomeModule;
import com.example.a20190312_lukestevens_nycschools.ui.home.di.HomeScope;
import com.example.a20190312_lukestevens_nycschools.ui.satRecord.SATRecordActivity;
import com.example.a20190312_lukestevens_nycschools.ui.satRecord.di.SATRecordModule;
import com.example.a20190312_lukestevens_nycschools.ui.satRecord.di.SATRecordScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {
    @ContributesAndroidInjector(modules = HomeModule.class)
    @HomeScope
    abstract HomeActivity homeActivity();

    @ContributesAndroidInjector(modules = SATRecordModule.class)
    @SATRecordScope
    abstract SATRecordActivity satRecordActivity();

}
