package com.example.a20190312_lukestevens_nycschools.di;

import android.app.Application;

import com.example.a20190312_lukestevens_nycschools.common.Constants;
import com.example.a20190312_lukestevens_nycschools.net.NYOpenDataService;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;
import com.example.a20190312_lukestevens_nycschools.repo.RemoteDataSource;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    @Provides
    @Singleton
    public Cache provideCache(Application application) {
        return new Cache(application.getCacheDir(), Constants.CACHE_SIZE);
    }

    @Provides
    @LoggingInterceptor
    @Singleton
    public Interceptor provideHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }


    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Cache cache,
                                            @LoggingInterceptor Interceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(Constants.API_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.API_TIMEOUT, TimeUnit.SECONDS)
                .cache(cache)
                .build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public NYOpenDataService provideNYOpenDataService(Retrofit retrofit) {
        return retrofit.create(NYOpenDataService.class);
    }

    @Provides
    @Remote
    @Singleton
    public DataSource provideRemoteDataSource(NYOpenDataService nyOpenDataService) {
        return new RemoteDataSource(nyOpenDataService);
    }

}
