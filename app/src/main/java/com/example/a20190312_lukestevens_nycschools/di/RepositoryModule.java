package com.example.a20190312_lukestevens_nycschools.di;

import com.example.a20190312_lukestevens_nycschools.repo.DataSource;
import com.example.a20190312_lukestevens_nycschools.repo.NYRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {
    @Provides
    @Repository
    @Singleton
    DataSource provideRepository(@Remote DataSource remoteDataSource, @Local DataSource localDataSource) {
        return new NYRepository(remoteDataSource, localDataSource);
    }

}
