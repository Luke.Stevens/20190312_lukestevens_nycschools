package com.example.a20190312_lukestevens_nycschools.di;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.a20190312_lukestevens_nycschools.common.Constants;
import com.example.a20190312_lukestevens_nycschools.db.AppDatabase;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;
import com.example.a20190312_lukestevens_nycschools.repo.LocalDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {
    @Provides
    @Singleton
    AppDatabase provideAppDatabase(Application application) {
        return Room.databaseBuilder(application, AppDatabase.class, Constants.DATABASE_NAME).build();
    }

    @Provides
    @Local
    @Singleton
    DataSource provideLocalDataSource(AppDatabase appDatabase) {
        return new LocalDataSource(appDatabase);
    }
}
