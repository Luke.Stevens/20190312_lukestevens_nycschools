package com.example.a20190312_lukestevens_nycschools.net;

import com.example.a20190312_lukestevens_nycschools.common.Constants;
import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.dto.SATRecord;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NYOpenDataService {
    @GET(Constants.HIGHSCHOOLS_ENDPOINT)
    Single<List<HighSchool>> getAllHighSchools();

    @GET(Constants.SATRECORDS_ENDPOINT)
    Single<List<SATRecord>> getSATRecord(@Query("dbn")String dbn);
}
