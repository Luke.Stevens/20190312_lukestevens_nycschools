package com.example.a20190312_lukestevens_nycschools.repo;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.dto.SATRecord;

import java.util.List;

import io.reactivex.Maybe;

public interface DataSource {
    /*
        When Flowable is used return type for Room queries, an empty database results in no
        emissions. Maybe handles the empty state in a nicer way
     */
    Maybe<List<HighSchool>> getAllHighSchools();
    void addHighSchools(HighSchool... highSchools);
    Maybe<SATRecord> getSATRecord(String dbn);
}
