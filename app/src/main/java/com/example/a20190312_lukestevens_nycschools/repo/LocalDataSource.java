package com.example.a20190312_lukestevens_nycschools.repo;

import com.example.a20190312_lukestevens_nycschools.db.AppDatabase;
import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.dto.SATRecord;

import java.util.List;

import io.reactivex.Maybe;

public class LocalDataSource implements DataSource {
    private final AppDatabase appDatabase;

    public LocalDataSource(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public Maybe<List<HighSchool>> getAllHighSchools() {
        return appDatabase.nyOpenDataDao().getAllHighSchools();
    }

    @Override
    public void addHighSchools(HighSchool... highSchools) {
        appDatabase.nyOpenDataDao().insertHighSchools(highSchools);
    }

    @Override
    public Maybe<SATRecord> getSATRecord(String dbn) {
        //NO-OP
        return null;
    }
}
