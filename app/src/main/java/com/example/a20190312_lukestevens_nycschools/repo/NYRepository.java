package com.example.a20190312_lukestevens_nycschools.repo;

import android.support.annotation.NonNull;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.dto.SATRecord;

import java.util.List;

import io.reactivex.Maybe;

public class NYRepository implements DataSource {

    private final DataSource remoteDataSource;
    private final DataSource localDataSource;

    public NYRepository(@NonNull DataSource remoteDataSource,
                        @NonNull DataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }

    /**
     * The method tries to get the data from the database, if no such data exists,
     * it tries to get it from the API and if successful, updates the database
     * @return An Observable of Results
     */

    @Override
    public Maybe<List<HighSchool>> getAllHighSchools() {
        return localDataSource.getAllHighSchools()
                .filter(result -> result.size() > 0)
                .switchIfEmpty(getRemoteData());
    }
    @Override
    public void addHighSchools(HighSchool... highSchools) {
        localDataSource.addHighSchools(highSchools);
    }

    @Override
    public Maybe<SATRecord> getSATRecord(String dbn) {
        return remoteDataSource.getSATRecord(dbn);
    }

    private Maybe<List<HighSchool>> getRemoteData() {
        return remoteDataSource.getAllHighSchools()
                .doOnSuccess(result -> addHighSchools(result.toArray(new HighSchool[0])));
    }
}
