package com.example.a20190312_lukestevens_nycschools.repo;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.dto.SATRecord;
import com.example.a20190312_lukestevens_nycschools.net.NYOpenDataService;

import java.util.List;

import io.reactivex.Maybe;

public class RemoteDataSource implements DataSource {

    private final NYOpenDataService nyOpenDataService;

    public RemoteDataSource(NYOpenDataService nyOpenDataService) {
        this.nyOpenDataService = nyOpenDataService;
    }

    @Override
    public Maybe<List<HighSchool>> getAllHighSchools() {
        return nyOpenDataService.getAllHighSchools()
                .flatMapMaybe(Maybe::just);
    }

    @Override
    public void addHighSchools(HighSchool... highSchools) {
        //NO-OP
    }

    //Using empty observable to detect unavailable records for a given dbn
    @Override
    public Maybe<SATRecord> getSATRecord(String dbn) {
        return nyOpenDataService.getSATRecord(dbn)
                .flatMapMaybe(result -> result.isEmpty() ? Maybe.empty() : Maybe.just(result.get(0)));
    }
}
