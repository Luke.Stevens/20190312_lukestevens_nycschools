package com.example.a20190312_lukestevens_nycschools.ui.home;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.a20190312_lukestevens_nycschools.R;
import com.example.a20190312_lukestevens_nycschools.common.Constants;
import com.example.a20190312_lukestevens_nycschools.databinding.ActivityHomeBinding;
import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.ui.satRecord.SATRecordActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class HomeActivity extends AppCompatActivity implements
        SchoolsAdapter.OnSchoolSelectedListener {

    @Inject
    HomeViewModel homeViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        ActivityHomeBinding activityHomeBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_home);
        activityHomeBinding.setProgressVisibility(homeViewModel.getProgressObservable());

        SchoolsAdapter schoolsAdapter = new SchoolsAdapter(this);
        RecyclerView recyclerView = activityHomeBinding.rvSchools;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(schoolsAdapter);

        homeViewModel.getHighSchoolsObservable().observe(this, schoolsAdapter::setData);
        homeViewModel.getErrorObservable().observe(this, this::showErrorMessage);
        homeViewModel.getAllSchools();
    }

    @Override
    public void onSchoolSelected(HighSchool highSchool) {
        Intent intent = new Intent(this, SATRecordActivity.class);
        intent.putExtra(Constants.HIGHSCHOOL_KEY, highSchool);
        startActivity(intent);
    }

    private void showErrorMessage(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
