package com.example.a20190312_lukestevens_nycschools.ui.home;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends ViewModel {
    private final MutableLiveData<List<HighSchool>> highSchoolsObservable;
    private final MutableLiveData<String> errorObservable;
    private final ObservableBoolean progressObservable;
    private final CompositeDisposable compositeDisposable;
    private final DataSource nyRepository;

    public HomeViewModel(DataSource nyRepository) {
        this.nyRepository = nyRepository;
        highSchoolsObservable = new MutableLiveData<>();
        errorObservable = new MutableLiveData<>();
        progressObservable = new ObservableBoolean(false);
        compositeDisposable = new CompositeDisposable();
    }

    public LiveData<List<HighSchool>> getHighSchoolsObservable() {
        return highSchoolsObservable;
    }

    public LiveData<String> getErrorObservable() {
        return errorObservable;
    }

    ObservableBoolean getProgressObservable() {
        return progressObservable;
    }

    public void getAllSchools() {
        compositeDisposable.add(nyRepository.getAllHighSchools()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> progressObservable.set(true))
                .doOnEvent((success, failure) -> progressObservable.set(false))
                .subscribe(this::handleSuccess, this::handleFailure));
    }

    private void handleSuccess(List<HighSchool> highSchools) {
        highSchoolsObservable.setValue(highSchools);
    }

    private void handleFailure(Throwable throwable) {
        throwable.printStackTrace();
        errorObservable.setValue(throwable.getMessage() == null ? "Unknown Error Happened"
                : throwable.getMessage());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
