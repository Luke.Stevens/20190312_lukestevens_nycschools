package com.example.a20190312_lukestevens_nycschools.ui.home;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.a20190312_lukestevens_nycschools.repo.DataSource;

public class HomeViewModelFactory implements ViewModelProvider.Factory {

    private final DataSource nyRepository;

    public HomeViewModelFactory(DataSource nyRepository) {
        this.nyRepository = nyRepository;
    }


    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(nyRepository);
        }
        throw new IllegalArgumentException("The class has to be an instance of: "
                + HomeViewModel.class.getSimpleName());
    }
}
