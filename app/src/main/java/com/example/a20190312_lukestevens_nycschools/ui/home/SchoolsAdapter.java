package com.example.a20190312_lukestevens_nycschools.ui.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.a20190312_lukestevens_nycschools.databinding.ItemSchoolBinding;
import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;

import java.util.ArrayList;
import java.util.List;

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.SchoolViewHolder> {

    private final List<HighSchool> highSchools;
    private static OnSchoolSelectedListener listener = null;

    public SchoolsAdapter(OnSchoolSelectedListener listener) {
        highSchools = new ArrayList<>();
        SchoolsAdapter.listener = listener;
    }

    void setData(List<HighSchool> data) {
        highSchools.clear();
        highSchools.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        return new SchoolViewHolder(ItemSchoolBinding.inflate(layoutInflater, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder schoolViewHolder, int position) {
        schoolViewHolder.bind(highSchools.get(position));
    }

    @Override
    public int getItemCount() {
        return highSchools.size();
    }

    static class SchoolViewHolder extends RecyclerView.ViewHolder {
        private final ItemSchoolBinding itemSchoolBinding;

        SchoolViewHolder(ItemSchoolBinding itemSchoolBinding) {
            super(itemSchoolBinding.getRoot());
            this.itemSchoolBinding = itemSchoolBinding;
        }

        void bind(HighSchool highSchool) {
            itemSchoolBinding.setHighSchool(highSchool);
            if (listener != null) {
                itemSchoolBinding.setListener(listener);
            }
            itemSchoolBinding.executePendingBindings();
        }
    }

    public interface OnSchoolSelectedListener {
        void onSchoolSelected(HighSchool highSchool);
    }
}
