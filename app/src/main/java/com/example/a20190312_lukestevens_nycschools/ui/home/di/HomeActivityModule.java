package com.example.a20190312_lukestevens_nycschools.ui.home.di;

import com.example.a20190312_lukestevens_nycschools.ui.home.HomeActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class HomeActivityModule {
    @HomeScope
    @Binds
    abstract HomeActivity provideHomeActivity(HomeActivity homeActivity);
}
