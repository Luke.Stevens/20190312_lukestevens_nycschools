package com.example.a20190312_lukestevens_nycschools.ui.home.di;

import android.arch.lifecycle.ViewModelProviders;

import com.example.a20190312_lukestevens_nycschools.di.Repository;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;
import com.example.a20190312_lukestevens_nycschools.ui.home.HomeActivity;
import com.example.a20190312_lukestevens_nycschools.ui.home.HomeViewModel;
import com.example.a20190312_lukestevens_nycschools.ui.home.HomeViewModelFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {
    @HomeScope
    @Provides
    public HomeViewModelFactory provideHomeViewModelFactory(
            @Repository DataSource nyRepository) {
        return new HomeViewModelFactory(nyRepository);
    }

    @HomeScope
    @Provides
    public HomeViewModel provideHomeViewModel(HomeActivity homeActivity,
                                              HomeViewModelFactory homeViewModelFactory) {
        return ViewModelProviders.of(homeActivity, homeViewModelFactory).get(HomeViewModel.class);
    }
}

