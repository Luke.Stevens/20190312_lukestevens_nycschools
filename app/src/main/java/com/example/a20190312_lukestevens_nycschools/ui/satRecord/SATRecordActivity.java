package com.example.a20190312_lukestevens_nycschools.ui.satRecord;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.a20190312_lukestevens_nycschools.R;
import com.example.a20190312_lukestevens_nycschools.common.Constants;
import com.example.a20190312_lukestevens_nycschools.databinding.ActivitySatRecordBinding;
import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class SATRecordActivity extends AppCompatActivity {

    @Inject
    SATRecordViewModel satRecordViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        ActivitySatRecordBinding binding = DataBindingUtil
                .setContentView(this, R.layout.activity_sat_record);
        binding.setProgressObservable(satRecordViewModel.getProgressObservable());
        binding.setErrorObservable(satRecordViewModel.getErrorObservable());
        satRecordViewModel.getSATRecordObservable().observe(this, binding::setSatRecord);

        satRecordViewModel.onDataRetrievedFromIntent(getIntent()
                .getParcelableExtra(Constants.HIGHSCHOOL_KEY));
    }
}
