package com.example.a20190312_lukestevens_nycschools.ui.satRecord;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.dto.SATRecord;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SATRecordViewModel extends ViewModel {

    private final MutableLiveData<SATRecord> satRecordObservable;
    private final ObservableBoolean progressObservable;
    private final ObservableBoolean errorObservable;
    private final CompositeDisposable compositeDisposable;
    private final DataSource nyRepository;

    public SATRecordViewModel(DataSource nyRepository) {
        this.nyRepository = nyRepository;
        satRecordObservable = new MutableLiveData<>();
        progressObservable = new ObservableBoolean(false);
        errorObservable = new ObservableBoolean(false);
        compositeDisposable = new CompositeDisposable();
    }

    ObservableBoolean getProgressObservable() {
        return progressObservable;
    }

    ObservableBoolean getErrorObservable() {
        return errorObservable;
    }

    LiveData<SATRecord> getSATRecordObservable() {
        return satRecordObservable;
    }

    void onDataRetrievedFromIntent(HighSchool highSchool) {
        if (highSchool != null) {
            compositeDisposable.add(nyRepository.getSATRecord(highSchool.getDbn())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(disposable -> progressObservable.set(true))
                    .doOnEvent((success, failure) -> progressObservable.set(false))
                    .defaultIfEmpty(new SATRecord())
                    .subscribe(this::handleSuccess, this::handleFailure));
        }
    }

    //If dbn is null that means we got zero results from the API for the given DBN
    private void handleSuccess(SATRecord satRecord) {
        if (satRecord != null && satRecord.getDbn() != null) {
            satRecordObservable.setValue(satRecord);
        } else {
            errorObservable.set(true);
        }
    }

    private void handleFailure(Throwable throwable) {
        throwable.printStackTrace();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
