package com.example.a20190312_lukestevens_nycschools.ui.satRecord;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.a20190312_lukestevens_nycschools.repo.DataSource;

public class SATRecordViewModelFactory implements ViewModelProvider.Factory {
    private final DataSource nyRepository;

    public SATRecordViewModelFactory(DataSource nyRepository) {
        this.nyRepository = nyRepository;
    }


    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SATRecordViewModel.class)) {
            return (T) new SATRecordViewModel(nyRepository);
        }
        throw new IllegalArgumentException("The class has to be an instance of: "
                + SATRecordViewModel.class.getSimpleName());
    }
}
