package com.example.a20190312_lukestevens_nycschools.ui.satRecord.di;

import com.example.a20190312_lukestevens_nycschools.ui.satRecord.SATRecordActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SATRecordActivityModule {
    @SATRecordScope
    @Binds
    abstract SATRecordActivity satRecordActivity(SATRecordActivity satRecordActivity);
}
