package com.example.a20190312_lukestevens_nycschools.ui.satRecord.di;

import android.arch.lifecycle.ViewModelProviders;

import com.example.a20190312_lukestevens_nycschools.di.Repository;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;
import com.example.a20190312_lukestevens_nycschools.ui.satRecord.SATRecordActivity;
import com.example.a20190312_lukestevens_nycschools.ui.satRecord.SATRecordViewModel;
import com.example.a20190312_lukestevens_nycschools.ui.satRecord.SATRecordViewModelFactory;

import dagger.Module;
import dagger.Provides;

@Module
public class SATRecordModule {
    @SATRecordScope
    @Provides
    public SATRecordViewModelFactory provideSATRecordViewModelFactory(
            @Repository DataSource nyRepository) {
        return new SATRecordViewModelFactory(nyRepository);
    }

    @SATRecordScope
    @Provides
    public SATRecordViewModel provideSATRecordViewModel(SATRecordActivity satRecordActivity,
                                                        SATRecordViewModelFactory satRecordViewModelFactory) {
        return ViewModelProviders.of(satRecordActivity, satRecordViewModelFactory).get(SATRecordViewModel.class);
    }
}
