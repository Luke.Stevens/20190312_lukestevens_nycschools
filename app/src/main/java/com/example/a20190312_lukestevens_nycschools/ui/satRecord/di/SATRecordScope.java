package com.example.a20190312_lukestevens_nycschools.ui.satRecord.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface SATRecordScope {
}
