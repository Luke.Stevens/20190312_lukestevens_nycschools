package com.example.a20190312_lukestevens_nycschools;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;
import com.example.a20190312_lukestevens_nycschools.ui.home.HomeViewModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomeViewModelTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Mock
    private DataSource repository;

    @Mock
    private Observer<List<HighSchool>> observer;

    @Mock
    private Observer<String> errorObserver;

    @Captor
    private ArgumentCaptor<List<HighSchool>> highSchoolsCaptor;

    private final List<HighSchool> highSchools = TestUtils.getHighSchools();

    private HomeViewModel homeViewModel;

    @BeforeClass
    public static void setupRxSchedulers() {
        Scheduler scheduler = new Scheduler() {
            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run, false);
            }
        };
        RxJavaPlugins.setInitIoSchedulerHandler(schedulerCallable -> scheduler);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> scheduler);
    }

    @Before
    public void setup() {
        homeViewModel = new HomeViewModel(repository);
    }

    @Test
    public void test_GetAllSchools_Success() {
        //Given
        when(repository.getAllHighSchools()).thenReturn(Maybe.just(highSchools));

        //When
        homeViewModel.getHighSchoolsObservable().observeForever(observer);
        homeViewModel.getAllSchools();

        //Then
        verify(observer, times(1)).onChanged(highSchoolsCaptor.capture());
        Assert.assertEquals(1, highSchoolsCaptor.getValue().size());
        Assert.assertEquals(TestUtils.SCHOOL_NAME_ONE,
                highSchoolsCaptor.getValue().get(0).getSchoolName());
    }

    @Test
    public void test_GetAllSchools_Failure() {
        //Given
        final String ERROR_MESSAGE = "TimeOut";
        when(repository.getAllHighSchools()).thenReturn(Maybe
                .error(new SocketTimeoutException(ERROR_MESSAGE)));

        //When
        homeViewModel.getErrorObservable().observeForever(errorObserver);
        homeViewModel.getHighSchoolsObservable().observeForever(observer);
        homeViewModel.getAllSchools();

        //Then
        verify(observer, times(0)).onChanged(anyList());
        verify(errorObserver).onChanged(ERROR_MESSAGE);

    }
}
