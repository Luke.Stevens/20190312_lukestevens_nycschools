package com.example.a20190312_lukestevens_nycschools;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;
import com.example.a20190312_lukestevens_nycschools.repo.DataSource;
import com.example.a20190312_lukestevens_nycschools.repo.NYRepository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NYRepositoryTest {

    @Mock
    private DataSource localDataSource;

    @Mock
    private DataSource remoteDataSource;

    @Captor
    private ArgumentCaptor<HighSchool> captor;

    private final List<HighSchool> highSchools = TestUtils.getHighSchools();

    private NYRepository nyRepository;

    @Before
    public void setup() {
        nyRepository = new NYRepository(remoteDataSource, localDataSource);
    }

    @Test
    public void test_DataRetrieval_EmptyDatabase() {
        //Given
        when(localDataSource.getAllHighSchools()).thenReturn(Maybe.empty());
        when(remoteDataSource.getAllHighSchools()).thenReturn(Maybe.just(highSchools));

        //When
        nyRepository.getAllHighSchools().subscribe();

        //Then
        InOrder inOrder = inOrder(localDataSource, remoteDataSource);
        inOrder.verify(localDataSource).getAllHighSchools();
        inOrder.verify(remoteDataSource).getAllHighSchools();
        inOrder.verify(localDataSource).addHighSchools(captor.capture());
        Assert.assertEquals(TestUtils.SCHOOL_NAME_ONE, captor.getValue().getSchoolName());
    }

    @Test
    public void test_DataRetrieval_ValidDatabase() {
        //Given
        when(localDataSource.getAllHighSchools()).thenReturn(Maybe.just(highSchools));
        when(remoteDataSource.getAllHighSchools()).thenReturn(Maybe.empty());

        //When
        TestObserver<List<HighSchool>> testSubscriber = nyRepository.getAllHighSchools().test();

        //Then
        testSubscriber.assertValueCount(1);
        testSubscriber.assertValueAt(0, highSchools);
    }
}
