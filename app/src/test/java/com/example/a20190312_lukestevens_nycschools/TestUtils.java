package com.example.a20190312_lukestevens_nycschools;

import com.example.a20190312_lukestevens_nycschools.dto.HighSchool;

import java.util.Collections;
import java.util.List;

public class TestUtils {
    static final String SCHOOL_NAME_ONE = "Test One";

    static List<HighSchool> getHighSchools() {
        HighSchool highSchool = new HighSchool();
        highSchool.setSchoolName(SCHOOL_NAME_ONE);
        return Collections.singletonList(highSchool);
    }
}
